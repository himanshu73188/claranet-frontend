import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NotesService } from 'src/app/notes/shared/notes.service';
import { LightboxService } from 'src/app/shared/lightbox.service';
import { UiService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-delete-note',
  templateUrl: './delete-note.component.html',
  styleUrls: ['./delete-note.component.scss']
})
export class DeleteNoteComponent implements OnInit {

  @Input() lightboxInfo: any;
  @Output() onClose                 = new EventEmitter();

  constructor(
    private ui              : UiService,
    private notes           : NotesService,
    private lightbox        : LightboxService
  ) { }

  ngOnInit(): void {
  }

  deleteNote() {
    
    // show message
    this.ui.showSnackbar("Deleting note... please wait", "pending");

    this.notes.deleteNote(this.lightboxInfo)
    .subscribe(data => {

        // console.log(data);
        this.ui.showSnackbar("Note deleted", "success");
        this.lightbox.reload("reloadNotes");
        this.lightbox.close();
        
    }, err => {

      // console.error('There was an error!', err);
      this.ui.showSnackbar("Oops an error occurred", "failure");

    });
  }
}
