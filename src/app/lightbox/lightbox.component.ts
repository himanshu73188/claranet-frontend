import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-lightbox',
  templateUrl: './lightbox.component.html',
  styleUrls: ['./lightbox.component.scss']
})
export class LightboxComponent implements OnInit {

  @Input() lightboxTemplate!: string;
  @Input() lightboxInfo: any;
  
  @Output() onClose   = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
