import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';

// services
import { AuthService } from './shared/auth.service';
import { UiService } from './shared/ui.service';

// modules
import { AppRoutingModule } from './app-routing.module';
import { AppMaterialModule } from './app-material.module';

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CreateNoteComponent } from './notes/create-note/create-note.component';
import { environment } from 'src/environments/environment';
import { NotesComponent } from './notes/notes.component';
import { LightboxComponent } from './lightbox/lightbox.component';
import { DeleteNoteComponent } from './lightbox/delete-note/delete-note.component';
import { CommonModule } from '@angular/common';
import { NoteListComponent } from './notes/note-list/note-list.component';
import { AsideComponent } from './aside/aside.component';
import { ViewNoteComponent } from './aside/view-note/view-note.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PageNotFoundComponent,
    CreateNoteComponent,
    NotesComponent,
    LightboxComponent,
    DeleteNoteComponent,
    NoteListComponent,
    AsideComponent,
    ViewNoteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AppMaterialModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  providers: [AuthService, UiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
