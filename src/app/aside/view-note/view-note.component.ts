import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Note } from 'src/app/notes/shared/notes.service';
import { DateService } from 'src/app/shared/util/date.service';

@Component({
  selector: 'app-view-note',
  templateUrl: './view-note.component.html',
  styleUrls: ['./view-note.component.scss']
})
export class ViewNoteComponent implements OnInit {

  @Output() onClose = new EventEmitter();
  @Input() asideId: any;

  // note form
  form!: FormGroup;
  
  // image preview
  imageUrl!:string | null;

  constructor(
    private fb              : FormBuilder,
    private date            : DateService
  ) { }

  ngOnInit(): void { 
    this.form = this.fb.group({
      title     : [{value : '', disabled: true}],
      body      : [{value : '', disabled: true}],
      media     : [{value : '', disabled: true}],
      status    : [{value : '', disabled: true}],
      created   : [{value : '', disabled: true}],
      edited    : [{value : '', disabled: true}],
    });
    console.log(this.form);
    this.noteDetails(this.asideId);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.noteDetails(changes.asideId.currentValue);
  }

  noteDetails(data:Note) {
    if(this.form) {
      this.form.controls.title.setValue(data?.title);
      this.form.controls.body.setValue(data?.body);
      this.form.controls.media.setValue(data?.media);
      this.form.controls.status.setValue(data?.status);
      this.form.controls.created.setValue(this.date.formatDate(data?.created));
      this.form.controls.edited.setValue(this.date.formatDate(data?.edited));
      this.imageUrl         = data?.media;
    }
  }

  // remove preview in case of any error
  removePreview(event:any) {
    this.imageUrl        =  null;
  }
}
