import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from 'src/app/app-material.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { validViewNote } from 'src/mocks';

import { ViewNoteComponent } from './view-note.component';

describe('ViewNoteComponent', () => {
  let component: ViewNoteComponent;
  let fixture: ComponentFixture<ViewNoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AppMaterialModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
      ],
      declarations: [ ViewNoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // update form values 
  function updateForm(title:string, body:string, media:string, status:string, created: Date, edited: Date) {
    component.form.controls['title'].setValue(title);
    component.form.controls['body'].setValue(body);
    component.form.controls['media'].setValue(media);
    component.form.controls['status'].setValue(status);
    component.form.controls['created'].setValue(created);
    component.form.controls['edited'].setValue(edited);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    expect(component.onClose).toBeDefined();
    expect(component.asideId).toBeUndefined();
    expect(component.form).toBeDefined();
    expect(component.form.status).toEqual('DISABLED');
    expect(component.imageUrl).toBeUndefined();
  });

  it('form values should update on noteDetails()', (() => {
    updateForm(validViewNote.title, validViewNote.body, validViewNote.media,  
    validViewNote.status, validViewNote.created, validViewNote.edited);
    expect(component.form.controls['title'].value).toEqual(validViewNote.title);
    expect(component.form.controls['body'].value).toEqual(validViewNote.body);
    expect(component.form.controls['media'].value).toEqual(validViewNote.media);
    expect(component.form.controls['status'].value).toEqual(validViewNote.status);
    expect(component.form.controls['created'].value).toEqual(validViewNote.created);
    expect(component.form.controls['edited'].value).toEqual(validViewNote.edited);
  }));
  
});
