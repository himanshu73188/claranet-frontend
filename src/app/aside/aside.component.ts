import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AsideService } from '../shared/aside.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent implements OnInit {

  asideTemplate: any;
  asideId: any;
  @Output() onClose = new EventEmitter();
  
  constructor(
    private aside: AsideService
  ) { }

  ngOnInit(): void {
    this.aside.getStatus()
      .subscribe(asideDetails => {
        this.asideId         = asideDetails.id;
        this.asideTemplate   = asideDetails.template;
    });
  }

}
