import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from './shared/auth.service';
import { filter } from 'rxjs/operators';
import { LightboxService } from './shared/lightbox.service';
import { AsideService } from './shared/aside.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'claranet-frontend';

  @ViewChild('sidenav', { static: false })
  public sidenav!: MatSidenav;

  @ViewChild('asidenav', { static: true })
  public asidenav!: MatSidenav;

  // you can also change mode to push or side
  menuside:MatDrawerMode      = 'over';
  menuopen                    = false;

  // Hide header
  showheader                  = false;

  // user details
  userinitials!: any;
  useremail!: any;

  // Public URLs like login
  publicUrl:any                = ['/login','/'];

  constructor(
    private auth        : AuthService,
    private router      : Router,
    public lightbox     : LightboxService,
    public aside        : AsideService
  ) { }

  ngOnInit() {

    this.aside.setSidenav(this.asidenav);

    this.auth.autoLogoutCheck();

    this.router.events
    .pipe(
      filter(e => e instanceof NavigationEnd)
    ).subscribe(data  => {

      if( this.publicUrl.includes( (data as any)['url'] ) ) {
        // if public url like login, hide the sidemenu & header
        this.menuopen       = false;
        this.showheader     = false;
      } else {
        // show header & sidenav
        this.showheader     = true;

        if ( ! this.auth.isLoggedIn() ) {
          this.router.navigate(["login"]);
        } else {
          // setup the profile details
          this.useremail      = this.auth.getUserEmail();
          this.userinitials   = this.useremail.substring(0, 1).toUpperCase();
        }
      }
    });

  }

  toggleMenu() {
    this.sidenav.toggle();
  }

  logOut() {
    this.auth.logout();
    this.sidenav.toggle();
  }


}
