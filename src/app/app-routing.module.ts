import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuardService as AuthGuard } from './login/shared/auth-guard.service';
import { CreateNoteComponent } from './notes/create-note/create-note.component';
import { NotesComponent } from './notes/notes.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'notes', canActivate: [AuthGuard], component: NotesComponent },
  { path: 'notes/create-note', canActivate: [AuthGuard], component: CreateNoteComponent },
  { path: 'notes/edit-note/:id', canActivate: [AuthGuard], component: CreateNoteComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
