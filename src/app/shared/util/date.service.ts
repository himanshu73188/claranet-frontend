import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {

  formatDate(date: string) {
    let months        = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let d             = new Date(date);
    let day           = d.getDate();
    let month         = months[d.getMonth()];
    let year          = d.getFullYear();
    return (day <= 9 ? '0' + day : day) + ' ' + month + ', ' + year;
  }
  
}