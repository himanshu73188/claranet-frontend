import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilService {
  private imageOrVideoFileTypes = [
    'image/gif',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
    'video/mp4',
    'video/x-m4v'
  ];

  validateFile(file: File): boolean {
    return this.imageOrVideoFileTypes.includes(file.type);
  }
}