import { Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsideService {

	asideSetting = {
		id: null,
		template: null
  };

  private subject = new Subject<any>();
  private asidenav!: MatSidenav;
  private reloadParent = new Subject<any>();

  public setSidenav(asidenav: MatSidenav) {
    this.asidenav = asidenav;
  }

  public open(id:any, template:any) {
    this.asideSetting = {
      id: id,
      template: template
    };
    this.subject.next(this.asideSetting);
    return this.asidenav.open();
  }

  public toggle(isOpen?: boolean) {
      return this.asidenav.toggle(isOpen);
  }

  public reload(template:string, data = {}) {
    const RELOAD_INFO = {
      template,
      data
    }
    this.reloadParent.next(RELOAD_INFO);
  }

  constructor() {
  }

  getStatus(): Observable<any> {
    return this.subject.asObservable();
  }

  closingAside() {
    this.asideSetting = {
      id: null,
      template: null
    };
    return this.asidenav.close();
	}

  setReload(): Observable<any> {
    return this.reloadParent.asObservable();
  }

}
