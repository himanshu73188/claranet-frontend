import { TestBed, inject } from '@angular/core/testing';
import { AppMaterialModule } from '../app-material.module';
import { UiService } from './ui.service';

describe('UiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppMaterialModule
      ],
      providers: [UiService]
    });
  });

  it('should be created', inject([UiService], (service: UiService) => {
    expect(service).toBeTruthy();
  }));
});
