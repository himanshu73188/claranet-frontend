import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LightboxService {

  showLightbox:boolean        = false;
  lightboxTemplate: string    = '';
  lightboxInfo: any;
  private reloadParent        = new Subject<any>();
  
  constructor() { }

  close() {
    this.showLightbox         = false;
  }

  public reload(template:string, data = {}) {
    const RELOAD_INFO = {
      template,
      data
    };
    this.reloadParent.next(RELOAD_INFO);
  }

  setReload(): Observable<any> {
    return this.reloadParent.asObservable();
  }
}
