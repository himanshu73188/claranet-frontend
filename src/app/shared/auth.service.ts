import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  // local storage
  private readonly storageKey: string               = "client_jwt_token";
  private readonly userId: string                   = 'client-manager-user-id';
  private readonly userEmail: string                = 'client-manager-user-email';
  private readonly tokenExpiration: string          = 'client-manager-user-expiration';
  private readonly tokenExpirationDate: string      = 'client-manager-user-expiration-date';

  // timer
  private tokenExpirationTimer: any;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) { }

  setToken (token: string){
    localStorage.setItem(this.storageKey, token);
	}

  getToken(){
		return localStorage.getItem(this.storageKey);
	}

  setUserEmail(email: string) {
    localStorage.setItem(this.userEmail, email);
  }

  getUserEmail() {
    return localStorage.getItem(this.userEmail);
  }

  setUserId(id: string) {
    localStorage.setItem(this.userId, id);
  }

  getUserId() {
    return localStorage.getItem(this.userId);
  }

  setTokenExpiration(expiration: string) {
    localStorage.setItem(this.tokenExpiration, expiration);
  }

  getTokenExpiration() {
    return localStorage.getItem(this.tokenExpiration);
  }

  setTokenExpiratioDate(expirationDate: string) {
    localStorage.setItem(this.tokenExpirationDate, expirationDate);
  }

  getTokenExpirationDate() {
    return localStorage.getItem(this.tokenExpirationDate);
  }

  isLoggedIn () {
		return this.getToken() !== null;
	}

  login(payload: {email: string, password: string, returnSecureToken: boolean}) {
    return this.http.post<any>(environment.loginUrl, payload);
  }

	logout() {

    // remove data from local storage
    localStorage.removeItem(this.storageKey);
    localStorage.removeItem(this.userEmail);
    localStorage.removeItem(this.tokenExpiration);
    localStorage.removeItem(this.tokenExpirationDate);
    localStorage.removeItem(this.userId);
    
    // route to login
    this.router.navigate(['/login']);

    // clear timer
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;

  }

  // logout after token expiration
  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  // check duration
  autoLogoutCheck() {
    let expirationDuration = this.getTokenExpiration();
    if (!expirationDuration) {
      return;
    }

    if (this.isLoggedIn()) {
      const expirationDuration = new Date(<string>this.getTokenExpirationDate()).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  // note- we can instead add auto login feature
  // firebase gives us refresh token, we can use that and do the auto login
  // but for this demo project, I am not adding auto login feature.
}
