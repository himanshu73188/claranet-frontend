import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../shared/auth.service';
import { UiService } from '../shared/ui.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // login form
  loginForm!: FormGroup;

  // Loading booleans
  isSubmitting              = false;

  // Template dynamic content
  loginbtn                  = 'Login';

  constructor(
    private fb        : FormBuilder,
    private router    : Router,
    private ui        : UiService,
    private auth      : AuthService
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email:    ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  submitLogin() {

    // payload
    const payload = {
      email               : this.loginForm.value.email,
      password            : this.loginForm.value.password,
      returnSecureToken   : true
    }

    // Update button and snackbar
    this.loginbtn               = "Logging in";
    this.isSubmitting           = true;
    this.ui.showSnackbar("Logging in... Please wait", "pending");

    this.auth.login(payload).subscribe({
      next: data => {
        
          // console.log(data);

          // Authenticate the user
          this.auth.setToken(data.idToken);
          this.auth.setUserId(data.localId);
          this.auth.setUserEmail(data.email);
          this.auth.setTokenExpiration(data.expiresIn);
          this.auth.setTokenExpiratioDate(new Date(new Date().getTime() + data.expiresIn * 1000).toString());
          this.auth.autoLogout(data.expiresIn * 1000);

          // Update button and snackbar
          this.loginbtn         = "Redirecting";
          this.ui.showSnackbar("Logged in successfully", "success");
          
          // Redirect to the notes page
          this.router.navigate(['/notes']);

      },
      error: error => {
          // console.error('There was an error!', error);
          this.isSubmitting     = false;
          this.loginbtn         = "Login";
          this.ui.showSnackbar("Sorry login details incorrect", "failure");
      }
    });

  }
}
