import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { blankUser, validUser } from 'src/mocks';
import { AppMaterialModule } from '../app-material.module';
import { AppRoutingModule } from '../app-routing.module';
import { AuthService } from '../shared/auth.service';

import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        AppRoutingModule,
        AppMaterialModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
      declarations: [ LoginComponent ],
      providers: [AuthService]
    })
    .compileComponents();
  });
  
  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // update form values 
  function updateForm(userEmail:string, userPassword:string) {
    component.loginForm.controls['email'].setValue(userEmail);
    component.loginForm.controls['password'].setValue(userPassword);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // initial state
  it('component initial state', () => {
    let btn = fixture.debugElement.query(By.css('.submit-button'));
    // btns
    expect(component.isSubmitting).toBeFalsy();
    expect(component.loginbtn).toEqual('Login');
    expect(btn.nativeElement.disabled).toBeTruthy();
    // form
    expect(component.loginForm).toBeDefined();
    expect(component.loginForm.invalid).toBeTruthy();
  });
  
  it('submitting button should be true when submitLogin()', () => {
    component.submitLogin();
    expect(component.isSubmitting).toBeTruthy();
    expect(component.loginbtn).toEqual('Logging in');
  });

  it('form value should update when u change the input', (() => {
    updateForm(validUser.username, validUser.password);
    expect(component.loginForm.controls['email'].value).toEqual(validUser.username);
    expect(component.loginForm.controls['password'].value).toEqual(validUser.password);
  }));

  it('Form invalid should be true when form is invalid', (() => {
    updateForm(blankUser.username, blankUser.password);
    expect(component.loginForm.invalid).toBeTruthy();
  }));

  it('should validate fields', (() => {
    // check form fields required validations 
    updateForm(blankUser.username, blankUser.password);
    expect(component.loginForm.controls['email'].valid).toBeFalsy();
    expect(component.loginForm.controls['password'].valid).toBeFalsy();
    updateForm(validUser.username, validUser.password);
    expect(component.loginForm.controls['email'].valid).toBeTruthy();
    expect(component.loginForm.controls['password'].valid).toBeTruthy();
    // check email validations 
    component.loginForm.controls['email'].setValue('testuser');
    expect(component.loginForm.controls['email'].valid).toBeFalsy();
    component.loginForm.controls['email'].setValue('testuser@todolist.com');
    expect(component.loginForm.controls['email'].valid).toBeTruthy();
  }));

}); 
