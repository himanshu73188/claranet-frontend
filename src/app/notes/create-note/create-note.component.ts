import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/auth.service';
import { StorageService } from 'src/app/shared/storage.service';
import { UiService } from 'src/app/shared/ui.service';
import { UtilService } from 'src/app/shared/util/util.service';
import { NotesService } from '../shared/notes.service';

@Component({
  selector: 'app-create-note',
  templateUrl: './create-note.component.html',
  styleUrls: ['./create-note.component.scss']
})
export class CreateNoteComponent implements OnInit {
 
  // note form
  form!: FormGroup;

  // Loading booleans
  isSubmitting:boolean      = false;

  // Template dynamic content
  btn:string                = 'Submit';

  // image preview
  imageUrl!:string | null;

  // edit page
  noteId!:string | null;
  isEditing:boolean         = false;
  isLoading:boolean         = false;
  note:any                  = [];

  constructor(
    private fb              : FormBuilder,
    private router          : Router,
    private ui              : UiService,
    private auth            : AuthService,
    private notes           : NotesService,
    private utilService     : UtilService,
    private storageService  : StorageService,
    private route           : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      title     : ['', Validators.required],
      body      : [''],
      media     : [''],
      status    : ['todo', Validators.required],
    });

    // check if edit page
    this.noteId             = this.route.snapshot.paramMap.get('id');
    if(this.noteId) {
      this.isEditing        = true;
      this.btn              = 'Update';
      this.fetchNote();
    }
  }
  
  uploadMedia(event: any) {
    
    // show message & disable submit btn
    this.ui.showSnackbar("Uploading media..please wait", "pending");
    this.isSubmitting              = true;

    if(event.target.files && event.target.files.length) {

      // file
      let fileData = event.target.files[0];
      let formData:any = new FormData();
      formData.append('file', fileData);

      if(formData.get('file').size > 1000000) {
        // display erorr
        this.ui.showSnackbar("Media should be less then 1MB", "failure");
      } else if(!this.utilService.validateFile(formData.get('file'))) {
        // display erorr
        this.ui.showSnackbar("Please upload supportive image/video format", "failure");
      } else {
        const mediaFolderPath                 = `media/${ this.auth.getUserId() }`;

        const { downloadUrl, uploadProgress } = this.storageService.uploadFileAndGetMetadata(
          mediaFolderPath,
          fileData,
        );

        downloadUrl.subscribe((downloadUrl) => {
          
          // set media value
          this.form.controls['media'].setValue(downloadUrl);
          this.imageUrl                  = downloadUrl;
          this.isSubmitting              = false;
          this.ui.showSnackbar("Media uploaded successfully", "success");

        }, err => {

          // console.error('There was an error!', err);
          this.ui.showSnackbar("Oops an error occurred", "failure");
    
        });
      }
    }
    
  }

  submit() {

    // payload
    let today                   = new Date().toISOString();
    const payload = {
      title             : this.form.value.title,
      body              : this.form.value.body,
      media             : this.form.value.media,
      status            : this.form.value.status,
      created           : today,
      edited            : today,
      deleted           : false,
      owner             : this.auth.getUserId()
    };

    // // create note
    let initialSnackbar:string   = 'Creating note... Please wait';
    let initialButton:string     = 'Submitting';
    let url:Observable<any>      = this.notes.createNote(payload);
    let finalSnackbar:string     = 'Note added successfully!';;
    let finalButton:string       = 'Submit';;
    
    // edit note
    if(this.isEditing) {
      const payload = {
        ...this.note,
        title             : this.form.value.title,
        body              : this.form.value.body,
        media             : this.form.value.media,
        status            : this.form.value.status,
        edited            : today,
      };
      initialSnackbar             = 'Updating note... Please wait';
      initialButton               = 'Updating';
      url                         = this.notes.updateNote(this.noteId, payload);
      finalSnackbar               = 'Note updated successfully!';
      finalButton                 = 'Update';
    }
    
    // Update button and snackbar
    this.ui.showSnackbar(initialSnackbar, 'pending');
    this.btn                    = initialButton;
    this.isSubmitting           = true;

    url
    .subscribe(data => {
        
        // console.log(data);
        // show success message
        this.ui.showSnackbar(finalSnackbar, 'success');
        // Redirect to the notes page
        this.router.navigate(['/notes']);

    }, err => {

      // console.error('There was an error!', err);
      this.isSubmitting     = false;
      this.btn              = finalButton;
      this.ui.showSnackbar("Oops an error occurred", "failure");

    });

  }


  // fetch note
  fetchNote() {
    this.isLoading            = true;
    this.notes.fetchNoteById(this.noteId)
    .subscribe(data => {
        
        // console.log(data);
        this.note            = data;
        this.form.controls.title.setValue(data.title);
        this.form.controls.body.setValue(data.body);
        this.form.controls.media.setValue(data.media);
        this.form.controls.status.setValue(data.status);
        this.imageUrl         = data.media;
        this.isLoading        = false;

    }, err => {

      // console.error('There was an error!', err);
      this.ui.showSnackbar("Oops an error occurred", "failure");

    });
  }

  // remove preview in case of any error
  removePreview(event:any) {
    this.imageUrl        =  null;
  }
  
}
