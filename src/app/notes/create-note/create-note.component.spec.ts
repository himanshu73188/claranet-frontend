import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from 'src/app/app-material.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AuthService } from 'src/app/shared/auth.service';
import { environment } from 'src/environments/environment';
import { validNote, blankNote } from 'src/mocks';
import { CreateNoteComponent } from './create-note.component';

describe('CreateNoteComponent', () => {
  let component: CreateNoteComponent;
  let fixture: ComponentFixture<CreateNoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        AppRoutingModule,
        AppMaterialModule,
        HttpClientModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        BrowserAnimationsModule
      ],
      declarations: [ CreateNoteComponent ],
      providers: [AuthService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // update form values 
  function updateForm(title:string, body:string, media:string, status:string) {
    component.form.controls['title'].setValue(title);
    component.form.controls['body'].setValue(body);
    component.form.controls['media'].setValue(media);
    component.form.controls['status'].setValue(status);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    let btn = fixture.debugElement.query(By.css('.submit-btn'));
    // btns
    expect(component.isSubmitting).toBeFalsy();
    expect(component.btn).toEqual('Submit');
    expect(btn.nativeElement.disabled).toBeTruthy();
    // booleans
    expect(component.isEditing).toBeFalsy();
    expect(component.isLoading).toBeFalsy();
    // form
    expect(component.form).toBeDefined();
    expect(component.form.invalid).toBeTruthy();
    // variables
    expect(component.noteId).toBeDefined();
    expect(component.note).toBeDefined();
    expect(component.imageUrl).toBeUndefined();
  });

  it('submitting button should be true when submit()', () => {
    component.submit();
    expect(component.isSubmitting).toBeTruthy();
  });

  it('button text should be Submitting when submit() as create note', (() => {
    component.submit();
    expect(component.btn).toEqual('Submitting');
  }));

  it('button text should be Updating when submit() as edit note', (() => {
    component.isEditing   = true;
    component.submit();
    expect(component.btn).toEqual('Updating');
  }));

  it('form value should update when u change the input', (() => {
    updateForm(validNote.title, validNote.body, validNote.media,  validNote.status);
    expect(component.form.controls['title'].value).toEqual(validNote.title);
    expect(component.form.controls['body'].value).toEqual(validNote.body);
    expect(component.form.controls['media'].value).toEqual(validNote.media);
    expect(component.form.controls['status'].value).toEqual(validNote.status);
  }));

  it('Form invalid should be true when form is invalid', (() => {
    updateForm(blankNote.title, blankNote.body, blankNote.media,  blankNote.status);
    expect(component.form.invalid).toBeTruthy();
  }));

  it('should validate fields', (() => {
    // check form fields required validations 
    updateForm(blankNote.title, blankNote.body, blankNote.media,  blankNote.status);
    expect(component.form.controls['title'].valid).toBeFalsy();
    expect(component.form.controls['status'].valid).toBeFalsy();
    updateForm(validNote.title, validNote.body, validNote.media,  validNote.status);
    expect(component.form.controls['title'].valid).toBeTruthy();
    expect(component.form.controls['status'].valid).toBeTruthy();
  }));

  it('component should show spinner when fetchNote()', (() => {
    component.fetchNote();
    expect(component.isLoading).toBeTruthy();
  }));

});
