import { AfterViewInit, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DateService } from 'src/app/shared/util/date.service';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnInit, AfterViewInit {

  @Input('notesData') notesData: any[]    = [];
  @Output() deleteNoteById                = new EventEmitter<string>();
  @Output() viewNoteById                  = new EventEmitter<string>();

  // table
  notes!: MatTableDataSource<any>;
  displayedColumns 					              = ['title', 'status', 'created', 'tools'];
  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  
  constructor(
    public date             : DateService,
  ) { }

  ngOnInit(): void {
    // data
    this.notes                    = new MatTableDataSource(this.notesData);
    //paginator
    this.notes.paginator          = this.paginator;
  }

  ngAfterViewInit() {
    // sort
    this.notes.sort               = this.sort;
  }

  deleteNote(id:string) {
    this.deleteNoteById.emit(id);
  }

  viewNote(id:string) {
    this.viewNoteById.emit(id);
  }

}
