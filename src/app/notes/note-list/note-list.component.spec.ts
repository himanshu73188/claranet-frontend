import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from 'src/app/app-material.module';

import { NoteListComponent } from './note-list.component';

describe('NoteListComponent', () => {
  let component: NoteListComponent;
  let fixture: ComponentFixture<NoteListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AppMaterialModule,
        BrowserAnimationsModule
      ],
      declarations: [ NoteListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    // variables
    expect(component.notesData).toBeDefined();
    expect(component.deleteNoteById).toBeDefined();
    expect(component.viewNoteById).toBeDefined();
    // table
    expect(component.notes).toBeDefined();
    expect(component.displayedColumns).toBeDefined();
    expect(component.paginator).toBeUndefined();
    expect(component.sort).toBeDefined();
  });



});
