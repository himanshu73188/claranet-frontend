import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { validNoteId } from 'src/mocks';
import { AppMaterialModule } from '../app-material.module';
import { AppRoutingModule } from '../app-routing.module';
import { AuthService } from '../shared/auth.service';

import { NotesComponent } from './notes.component';

describe('NotesComponent', () => {
  let component: NotesComponent;
  let fixture: ComponentFixture<NotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AppMaterialModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule
      ],
      declarations: [ NotesComponent ],
      providers: [AuthService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('component initial state', () => {
    // booleans
    expect(component.isLoading).toBeTruthy();
    // variables
    expect(component.notesData).toBeUndefined();
    expect(component.userId).toBeNull();
  });

  it('component should show spinner when fetchNotes()', (() => {
    component.fetchNotes();
    expect(component.isLoading).toBeTruthy();
  }));

  it('component should open lightbox when deleteNote()', (() => {
    component.deleteNote(validNoteId.id);
    expect(component.lightbox.showLightbox).toBeTruthy();
    expect(component.lightbox.lightboxInfo).toEqual(validNoteId.id);
    expect(component.lightbox.lightboxTemplate).toEqual('deleteNote');
  }));

});
