import { Component, OnInit } from '@angular/core';
import { AsideService } from '../shared/aside.service';
import { AuthService } from '../shared/auth.service';
import { LightboxService } from '../shared/lightbox.service';
import { UiService } from '../shared/ui.service';
import { NotesService } from './shared/notes.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {
  
  // booleans
  isLoading                           = true;

  // table
  notesData: any;

  // auth
  userId!:string | null;
  
  constructor(
    private ui              : UiService,
    private auth            : AuthService,
    private notes           : NotesService,
    public lightbox        : LightboxService,
    private aside           : AsideService
  ) { }

  ngOnInit(): void {
    
    this.userId = this.auth.getUserId();
    this.fetchNotes();

    // reload notes
    this.lightbox.setReload()
    .subscribe( res => {
      if( res && res.hasOwnProperty('template') && res['template'] && res['template'] == "reloadNotes" ) {
        this.fetchNotes();
      }
    });

  }

  fetchNotes() {

    this.isLoading            = true;

    this.notes.fetchNotes(this.userId)
    .subscribe(data => {

        // console.log(data);
        this.notesData                = data;
        this.isLoading                = false;
        
    }, err => {

      // console.error('There was an error!', err);
      this.ui.showSnackbar("Oops an error occurred", "failure");

    });

  }

  deleteNote(id:string) {
    this.lightbox.showLightbox        = true;
    this.lightbox.lightboxInfo        = id;
    this.lightbox.lightboxTemplate    = 'deleteNote';
  }

  openAside(id:any, template:string) {
    this.aside.open(id, template);
  }

}
