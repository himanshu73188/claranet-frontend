import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { AuthService } from "src/app/shared/auth.service";
import { environment } from "src/environments/environment";

export interface Note {
  title     : string;
  body      : string;
  media     : string;
  status    : string;
  created   : string;
  edited    : string;
  deleted   : boolean;
  owner     : string | any;
}

@Injectable({ providedIn: 'root' })
export class NotesService {

  constructor(
    private http      : HttpClient,
    private auth      : AuthService,
  ) { }
  
  createNote(payload: Note) {
    return this.http.post<any>(environment.apiUrl + '/notes.json?auth=' + this.auth.getToken(), payload);
  }
  
  fetchNotes(userId: string | null) {
    const queryParams = "?auth=" + this.auth.getToken() + '&orderBy="owner"&equalTo="' + userId + '"';
    return this.http.get<any>(environment.apiUrl + '/notes.json' + queryParams)
    .pipe(
      map(response => 
        Object.keys(response).map(note => {
          return {...response[note], id: note} 
        }).filter(note => !note.deleted)
      )
    );
  } 

  fetchNoteById(id: string | null) {
    return this.http.get<any>(environment.apiUrl + '/notes/' + id + '.json?auth=' + this.auth.getToken());
  }

  deleteNote(id: string) {
    let payload = { deleted: true };
    return this.http.patch<any>(environment.apiUrl + '/notes/' + id + '.json?auth=' + this.auth.getToken(), payload);
  }

  updateNote(id: string| null, payload: Note) {
    return this.http.put<any>(environment.apiUrl + '/notes/' + id + '.json?auth=' + this.auth.getToken(), payload);
  }
}