export const blankUser = {
  username: '',
  password: ''
};

export const validUser = {
  username: '12345@todolist.com',
  password: '123456'
};

export const blankNote = {
  title   : '',
  body    : '',
  media   : '',
  status  : '',
};

export const validNote = {
  title   : 'test title',
  body    : 'test body',
  media   : 'https://dummyimage.com/640x360/fff/aaa',
  status  : 'todo',
};

export const validViewNote = {
  title   : 'test title',
  body    : 'test body',
  media   : 'https://dummyimage.com/640x360/fff/aaa',
  status  : 'todo',
  created : new Date("2021-01-17T16:37:59.861Z"),
  edited  : new Date("2021-01-17T16:37:59.861Z"),
};

export const validNoteId = {
  id   : 'ABC123',
};