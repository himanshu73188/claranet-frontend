// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://claranet-frontend-default-rtdb.firebaseio.com/',
  loginUrl: 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAEsaygySa4Oc_gsSAG9E45eGnISMAoA6g',
  firebaseConfig: {
    apiKey: "AIzaSyAEsaygySa4Oc_gsSAG9E45eGnISMAoA6g",
    authDomain: "claranet-frontend.firebaseapp.com",
    databaseURL: "https://claranet-frontend-default-rtdb.firebaseio.com",
    projectId: "claranet-frontend",
    storageBucket: "claranet-frontend.appspot.com",
    messagingSenderId: "212156515257",
    appId: "1:212156515257:web:88de64bf8e7de44b6bec68",
    measurementId: "G-VGYBQJWWSY"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
