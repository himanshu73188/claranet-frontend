export const environment = {
  production: true,
  apiUrl: 'https://claranet-frontend-default-rtdb.firebaseio.com/',
  loginUrl: 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAEsaygySa4Oc_gsSAG9E45eGnISMAoA6g',
  firebaseConfig: {
    apiKey: "AIzaSyAEsaygySa4Oc_gsSAG9E45eGnISMAoA6g",
    authDomain: "claranet-frontend.firebaseapp.com",
    databaseURL: "https://claranet-frontend-default-rtdb.firebaseio.com",
    projectId: "claranet-frontend",
    storageBucket: "claranet-frontend.appspot.com",
    messagingSenderId: "212156515257",
    appId: "1:212156515257:web:88de64bf8e7de44b6bec68",
    measurementId: "G-VGYBQJWWSY"
  }
};
